/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.apache.giraph.examples;

import org.apache.giraph.aggregators.DoubleMaxAggregator;
import org.apache.giraph.aggregators.DoubleMinAggregator;
import org.apache.giraph.aggregators.LongSumAggregator;
import org.apache.giraph.edge.Edge;
import org.apache.giraph.edge.EdgeFactory;
import org.apache.giraph.graph.BasicComputation;
import org.apache.giraph.graph.Vertex;
import org.apache.giraph.io.VertexReader;
import org.apache.giraph.io.formats.TextVertexOutputFormat;
import org.apache.giraph.master.DefaultMasterCompute;
import org.apache.giraph.worker.WorkerContext;
import org.apache.hadoop.io.DoubleWritable;
import org.apache.hadoop.io.FloatWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.InputSplit;	
import org.apache.hadoop.mapreduce.TaskAttemptContext;
import org.apache.log4j.Logger;
import org.apache.giraph.io.formats.TextVertexInputFormat;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.NullWritable;
import java.util.Hashtable;
import techniques.PL.*;


import java.io.IOException;
import java.util.List;


import com.google.common.collect.Lists;

import java.util.regex.Pattern;

/**
 * Demonstrates the basic Pregel PageRank implementation.
 */
@Algorithm(
		name = "SAT solver with Pregel"
		)
public class MySATSolver extends BasicComputation<IntWritable,
IntWritable, NullWritable, IntWritable> {
	/** Number of supersteps for this test */
	public static final int MAX_SUPERSTEPS = 3;
	/** Logger */
	private static final Logger LOG =
			Logger.getLogger(MySATSolver.class);
	/** Sum aggregator name */
	private static String SUM_AGG = "sum";
	/** Min aggregator name */
	private static String MIN_AGG = "min";
	/** Max aggregator name */
	private static String MAX_AGG = "max";
	
	private static Hashtable<Integer, Integer> componentMap = new Hashtable<Integer, Integer>();

	private static boolean satisfiable = true;
	
	private void isSatisfiable(Integer id, Integer component){
		// check if the negation of current vertex is in the same component
		if (component==componentMap.get(-id)){
			LOG.error("UNSAT by " + Math.abs(id));
			System.out.println("UNSAT by " + Math.abs(id));
			satisfiable = false;
		} else {
			LOG.error("map put: " + id + ", "+ component);
			componentMap.put(id, component);
		}
	}

	@Override
	public void compute(
			Vertex<IntWritable, IntWritable, NullWritable> vertex,
			Iterable<IntWritable> messages) throws IOException {
		int currentComponent = vertex.getValue().get();


		if (satisfiable==true){
			if (LOG.isDebugEnabled()){
				LOG.error("super step "+getSuperstep());
				LOG.error("current vertex " + vertex.getId() + ", current component" + currentComponent);
			}
			
			// First superstep is special, because we can simply look at the neighbors
			if (getSuperstep() == 0) {
				componentMap.put(currentComponent, currentComponent);
				for (Edge<IntWritable, NullWritable> edge : vertex.getEdges()) {
					int neighbor = edge.getTargetVertexId().get();
					if (neighbor < currentComponent) {
						currentComponent = neighbor;
					}
					if (LOG.isDebugEnabled()){
						LOG.error("neighbor "+neighbor+", currentComponet"+currentComponent);
					}
				}
				// Only need to send value if it is not the own id
				if (currentComponent != vertex.getValue().get()) {
					isSatisfiable(vertex.getId().get(), currentComponent);
					vertex.setValue(new IntWritable(currentComponent));


					for (Edge<IntWritable, NullWritable> edge : vertex.getEdges()) {
						IntWritable neighbor = edge.getTargetVertexId();
						if (neighbor.get() > currentComponent) {
							if (LOG.isDebugEnabled()){
								LOG.error("update neighbor"+ neighbor+" with currentComponent" + currentComponent);
							}
							sendMessage(neighbor, vertex.getValue());
						}
					}
				}

				vertex.voteToHalt();
				return;
			}

			boolean changed = false;
			// did we get a smaller id ?
			for (IntWritable message : messages) {
				int candidateComponent = message.get();
				if (candidateComponent < currentComponent) {
					currentComponent = candidateComponent;
					changed = true;
				}
			}
			if (LOG.isDebugEnabled()){
				LOG.error("changed "+changed);
			}

			// propagate new component id to the neighbors
			if (changed) {
				isSatisfiable(vertex.getId().get(), currentComponent);
				vertex.setValue(new IntWritable(currentComponent));
				sendMessageToAllEdges(vertex, vertex.getValue());
			}
		}
		vertex.voteToHalt();
	}
	
	
	/**
	 * Worker context used with {@link MySATSolver}.
	 */
	public static class MySATSolverWorkerContext extends
	WorkerContext {
		/** Final max value for verification for local jobs */
		private static double FINAL_MAX;
		/** Final min value for verification for local jobs */
		private static double FINAL_MIN;
		/** Final sum value for verification for local jobs */
		private static long FINAL_SUM;
		
		
		public static double getFinalMax() {
			return FINAL_MAX;
		}

		public static double getFinalMin() {
			return FINAL_MIN;
		}

		public static long getFinalSum() {
			return FINAL_SUM;
		}

		@Override
		public void preApplication()
				throws InstantiationException, IllegalAccessException {
			LOG.error("preapplication");
			
		}

		@Override
		public void postApplication() {
			LOG.error("postapplication");
			System.out.println("SAT");
		}

		@Override
		public void preSuperstep() {
			LOG.error("preSuperstep");
		}

		@Override
		public void postSuperstep() { 
			LOG.error("postSuperstep");
		}
	}

	/**
	 * Master compute associated with {@link MySATSolver}.
	 * It registers required aggregators.
	 */
	public static class MySATSolverMasterCompute extends
	DefaultMasterCompute {
		@Override
		public void initialize() throws InstantiationException,
		IllegalAccessException {
		}
	}


	/**
	 * Simple VertexInputFormat that supports {@link MySATSolver}
	 */
	public static class MySATSolverVertexInputFormat extends
	TextVertexInputFormat<IntWritable, IntWritable, NullWritable> {
		/** Separator of the vertex and neighbors */
		private static final Pattern SEPARATOR = Pattern.compile("[\t ]");

		@Override
		public TextVertexReader createVertexReader(InputSplit split,
				TaskAttemptContext context)
						throws IOException {
			return new MySATSolverVertexReader();
		}
		
		
		/**
		 * Vertex reader associated with {@link IntIntNullTextInputFormat}.
		 */
		public class MySATSolverVertexReader extends
		TextVertexReaderFromEachLineProcessed<String[]> {
			/**
			 * Cached vertex id for the current line
			 */
			private IntWritable id;
			
			@Override
			protected String[] preprocessLine(Text line) throws IOException {
				LOG.error("line: " + line.toString());
				String[] tokens = SEPARATOR.split(line.toString());
				id = new IntWritable(Integer.parseInt(tokens[0]));
				return tokens;
			}

			@Override
			protected IntWritable getId(String[] tokens) throws IOException {
				return id;
			}

			@Override
			protected IntWritable getValue(String[] tokens) throws IOException {
				return id;
			}

			@Override
			protected Iterable<Edge<IntWritable, NullWritable>> getEdges(
					String[] tokens) throws IOException {
				List<Edge<IntWritable, NullWritable>> edges =
						Lists.newArrayListWithCapacity(tokens.length - 1);
				for (int n = 1; n < tokens.length; n++) {
					edges.add(EdgeFactory.create(
							new IntWritable(Integer.parseInt(tokens[n]))));
				}
				return edges;
			}
		}
	}
	

	/**
	 * Simple VertexOutputFormat that supports {@link MySATSolver}
	 */
	public static class MySATSolverVertexOutputFormat extends
	TextVertexOutputFormat<IntWritable, IntWritable, NullWritable> {
		@Override
		public TextVertexWriter createVertexWriter(TaskAttemptContext context)
				throws IOException, InterruptedException {
			return new MySATSolverVertexWriter();
		}

		/**
		 * Simple VertexWriter that supports {@link MySATSolver}
		 */
		public class MySATSolverVertexWriter extends TextVertexWriter {
			@Override
			public void writeVertex(
					Vertex<IntWritable, IntWritable, NullWritable> vertex)
							throws IOException, InterruptedException {
				getRecordWriter().write(
						new Text(vertex.getId().toString()),
						new Text(vertex.getValue().toString())
						);
				
			}
		}
	}
}

